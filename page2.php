<?php
session_start();
class Question {
    public $ques_number;
    public $question;
    public $ans_A;
    public $ans_B;
    public $ans_C;
    public $ans_D;
    public $answer;

    public function __construct($ques_number, $question, $ans_A, $ans_B, $ans_C, $ans_D, $answer){
        $this->ques_number = $ques_number;
        $this->question = $question;
        $this->ans_A = $ans_A;
        $this->ans_B = $ans_B;
        $this->ans_C = $ans_C;
        $this->ans_D = $ans_D;
        $this->answer = $answer;
    }
}

$questions_list = [
    $ques_1 = new Question(6, "PHP stands for -", "Hypertext Preprocessor", "Pretext Hypertext Preprocessor", "Personal Home Processor", "None of the above", 1),
    $ques_2 = new Question(7, "Who is known as the father of PHP?", "Drek Kolkevi", "List Barely", "Rasmus Lerdrof", "None of the above", 1),
    $ques_3 = new Question(8, "Variable name in PHP starts with -", "! (Exclamation)", "$ (Dollar)", "& (Ampersand)", "# (Hash)", 1),
    $ques_4 = new Question(9, "Which of the following is the default file extension of PHP?
    ", ".php", ".hphp", ".xml", ".html", 1),
    $ques_5 = new Question(10, "Which of the following is not a variable scope in PHP?", "Extern", "Local", "Static", "Global", 1),
];

$cookie_duration = strtotime('+30 days');

if(isset($_POST['next'])){
    foreach($_POST as $key => $value) {
        if ($key != 'next'){
           setcookie($key, $value, $cookie_duration);
        }
    }
}

// var_dump($_COOKIE)

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" />
    <form action="scores.php" method="post">
    <title>Document</title>
</head>
<body>
    <form action="" method="post">
        <div class="container">
            <?php
            foreach ($questions_list as $ques){
                echo ('
                <h3>Question '.$ques->ques_number.': '.$ques->question.'</h3>
                <div>
                    <input type="radio" name="ques_'.$ques->ques_number.'" value="1">  '.$ques->ans_A.'
                </div>
                <div>
                    <input type="radio" name="ques_'.$ques->ques_number.'" value="2">  '.$ques->ans_B.'
                </div>
                <div>
                    <input type="radio" name="ques_'.$ques->ques_number.'" value="3">  '.$ques->ans_C.'
                </div>   
                <div>
                    <input type="radio" name="ques_'.$ques->ques_number.'" value="4">  '.$ques->ans_D.'
                </div>
                ');
            }
            ?>
            <div class="form-group">
                <input type="submit" value="Submit" name="submit" class="btn btn-primary"/>
            </div>
        </div>
    </form>
</body>
</html>